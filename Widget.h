/*
    Copyright (C) 2011 Harald Sitter <sitter@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), Nokia Corporation
    (or its successors, if any) and the KDE Free Qt Foundation, which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WIDGET_H
#define WIDGET_H

#include <QtGui/QWidget>

class Widget : public QWidget
{
    Q_OBJECT
    // Fake properties to allow QMetaEnum inspection of the Qt namespace enums.
    // Qt namespace enums are processed into a Qt class by MOC, which is however
    // not accessible at runtime. By introducing a fake property we can access
    // the QMetaEnum via the property.
    // Mind that this is the only sane way to do it. Q_ENUMS would not work as
    // a foreign class needed to have a Q_OBJECT macro (which Qt does not have)
    // and more importantly it would require the foreign structure to be a class
    // but outside moc Qt is a namespace actually.
    Q_PROPERTY(Qt::Key keyProperty)
    Q_PROPERTY(Qt::Orientation orientationProperty)
    // Notice the *s*, only the flags are registered, not the enum.
    Q_PROPERTY(Qt::KeyboardModifiers keyModifiersProperty)
    Q_PROPERTY(Qt::MouseButtons mouseButtonsProperty)
public:
    Widget(QWidget *parent = 0);
    ~Widget();

    bool event(QEvent *e);
};

QDebug operator<<(QDebug dbg, const QKeyEvent &e);
QDebug operator<<(QDebug dbg, const QMouseEvent &e);
QDebug operator<<(QDebug dbg, const QWheelEvent &e);

#endif // WIDGET_H
