/*
    Copyright (C) 2011 Harald Sitter <sitter@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), Nokia Corporation
    (or its successors, if any) and the KDE Free Qt Foundation, which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Widget.h"

#include <QtCore/QDebug>
#include <QtCore/QEvent>
#include <QtCore/QMetaEnum>
#include <QtCore/QMetaObject>
#include <QtGui/QKeyEvent>
#include <QtGui/QWheelEvent>

inline static QMetaProperty findProperty(const QMetaObject &meta, const char *property)
{
    for (int i = 0; i < meta.propertyCount(); ++i) {
        QMetaProperty p = meta.property(i);
        if (qstrcmp(p.name(), property) == 0)
            return p;
    }
}

template<typename Enum>
inline static QStringList valueList(const QMetaEnum &metaEnum, const QFlags<Enum> &flags)
{
    QStringList list;
    for (int i = 0; i < metaEnum.keyCount(); ++i) {
        if (flags & metaEnum.value(i))
            list.append(QLatin1String(metaEnum.key(i)));
    }
    return list;
}

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
}

Widget::~Widget()
{
}

bool Widget::event(QEvent *e)
{
    switch (e->type()) {
    case QEvent::KeyPress:
    case QEvent::KeyRelease:
    case QEvent::MouseButtonDblClick:
    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonRelease:
    case QEvent::MouseMove:
    case QEvent::Wheel:
        break;
    default:
        return QWidget::event(e);
    }

    qDebug() << "---------------------";

    if (!e)
        qDebug() << "Got NULL event";
    else {
        QMetaObject meta = e->staticMetaObject;
        for (int i = 0; i < meta.enumeratorCount(); ++i) {
            QMetaEnum m = meta.enumerator(i);
            if (m.name() == QLatin1String("Type")) {
                qDebug() << m.valueToKey(e->type()) << "event";
                break;
            }
        }

        switch (e->type()) {
        case QEvent::KeyPress:
        case QEvent::KeyRelease:
            qDebug() << *(QKeyEvent*)e; break;
        case QEvent::MouseButtonDblClick:
        case QEvent::MouseButtonPress:
        case QEvent::MouseButtonRelease:
        case QEvent::MouseMove:
            qDebug() << *(QMouseEvent*)e; break;
        case QEvent::Wheel:
            qDebug() << *(QWheelEvent*)e; break;
        }
    }

    return QWidget::event(e);
}

QDebug operator<<(QDebug dbg, const QKeyEvent &e)
{
    QMetaObject meta = Widget::staticMetaObject;
    QMetaEnum keyEnum = findProperty(meta, "keyProperty").enumerator();

    // Build modifier string list
    QStringList modifierList = valueList(
                findProperty(meta, "keyModifiersProperty").enumerator(),
                e.modifiers());

    dbg.nospace() << "    "
                  << "count " << e.count()
                  << ", key " <<
                     (qstrlen(keyEnum.valueToKey(e.key())) > 0
                      ? keyEnum.valueToKey(e.key())
                      : "*Not supported by Qt*")
                  << ", modifiers " << modifierList
                  << "\n";

    dbg.nospace() << "   "
                  << "native scan code " << e.nativeScanCode()
                  << ", native modifiers " << e.nativeModifiers();

    return dbg.space();
}

QDebug operator<<(QDebug dbg, const QMouseEvent &e)
{
    QMetaObject meta = Widget::staticMetaObject;
    QMetaEnum buttonsEnum = findProperty(meta, "mouseButtonsProperty").enumerator();

    // Build buttons and modifier string lists
    QStringList buttonsList = valueList(buttonsEnum, e.buttons());
    QStringList modifierList = valueList(
                findProperty(meta, "keyModifiersProperty").enumerator(),
                e.modifiers());

    dbg.nospace() << "    "
                  << "cause " << e.button()
                  << ", buttons " << buttonsList
                  << ", globalPos " << e.globalPos()
                  << ", pos " << e.pos()
                  << ", modifiers " << modifierList
                  ;

    return dbg.space();
}

QDebug operator<<(QDebug dbg, const QWheelEvent &e)
{
    QMetaObject meta = Widget::staticMetaObject;
    QMetaEnum buttonsEnum = findProperty(meta, "mouseButtonsProperty").enumerator();
    QMetaEnum orientationEnum = findProperty(meta, "orientationProperty").enumerator();

    // Build buttons and modifier string lists
    QStringList buttonsList = valueList(buttonsEnum, e.buttons());
    QStringList modifierList = valueList(
                findProperty(meta, "keyModifiersProperty").enumerator(),
                e.modifiers());

    dbg.nospace() << "    "
                  << "orientation " << orientationEnum.valueToKey(e.orientation())
                  << ", rotation delta " << e.delta()
                  << ", buttons " << buttonsList
                  << ", globalPos " << e.globalPos()
                  << ", pos " << e.pos()
                  << ", modifiers " << modifierList
                  ;

    return dbg.space();
}
